use crate::feedyaml::FeedConfig;
use crate::logyaml::LogConfig;
use rustcatcher::feedyaml;
use rustcatcher::logyaml;

fn main() {
    // use walkerdir to find .mp3 files in current dir and create struct
    let logconfig1 = match LogConfig::new() {
        Ok(logconfig) => {
            println!("creating log of current mp3s in directory...");
            logconfig
        }
        Err(e) => {
            println!("walkdir failed to scan dir: {}", e);
            return;
        }
    };

    // read in the feed config yaml file and create struct of feed info
    let feedconfig1 = match FeedConfig::new() {
        Ok(feedconfig) => {
            println!("reading config yaml file...");
            feedconfig
        }
        Err(e) => {
            println!("Couldn't find feedconfig.yml: {}", e);
            return;
        }
    };

    // use rss fetch titles and items(names and url) to be downloaded
    // create struct of info
    let mut title_items1 = match feedconfig1.get_feed_urls(&logconfig1) {
        Ok(title_items) => {
            println!("fetching rss feeds...");
            title_items
        }
        Err(e) => {
            println!("Couldn't fetch rss feeds: {}", e);
            return;
        }
    };
    //download and save items in the current folder
    title_items1.download();
}
