//
// feedconfig.rs
// Copyright (C) 2019 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
// extern crate yaml_rust;
use crate::logyaml::LogConfig;
use rss::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::Error;

#[derive(Debug, Serialize, Deserialize)]
pub struct FeedConfig {
    pub feeds: Vec<HashMap<String, String>>,
}

#[derive(Debug)]
pub struct TitlesItems {
    // pub title: Vec<String>,
    pub item: Vec<HashMap<String, String>>,
}

impl FeedConfig {
    pub fn new() -> Result<FeedConfig, Error> {
        let yaml = "feedconfig.yml";
        let mut handle = match File::open(yaml) {
            Ok(file) => file,
            Err(e) => return Err(e),
        };

        let mut config_string = String::new();
        handle
            .read_to_string(&mut config_string)
            .expect("unable to read rss config");
        // println!("{:?}", config_string);
        let feed_config1: FeedConfig = serde_yaml::from_str(&config_string).unwrap();

        Ok(feed_config1)
    }
    pub fn get_feed_urls(self, log: &LogConfig) -> Result<TitlesItems, rss::Error> {
        // let mut title_vec = Vec::new();
        // let mut name_url_hash = HashMap::new();
        let mut item_vec = Vec::new();

        for feed in self.feeds {
            let channel = match Channel::from_url(feed.get("rss").unwrap()) {
                Ok(file) => file,
                Err(e) => return Err(e),
            };
            let title = channel.title().to_string();
            // title_vec.push(title);

            let url_vec: Vec<String> = channel
                .items()
                .iter()
                .take(2)
                .map(|x| x.enclosure().unwrap().url().to_string())
                .collect();

            let mut name_vec: Vec<String> = channel
                .items()
                .iter()
                .take(2)
                .map(|x| x.title().unwrap().to_string())
                .collect();

            name_vec = name_vec
                .iter()
                .map(|x| {
                    x.split_whitespace()
                        .collect::<Vec<&str>>()
                        .join("")
                        .to_string()
                })
                .collect();

            name_vec.iter_mut().for_each(|x| x.push_str(".mp3"));

            let mut name_url_hash: HashMap<String, String> =
                name_vec.into_iter().zip(url_vec.into_iter()).collect();

            name_url_hash.insert("title".to_string(), title);

            // println!("name_url_hash: {:?}", &name_url_hash);

            name_url_hash = name_url_hash
                .into_iter()
                .filter(|(name, _url)| !log.log_vec.contains(name))
                .collect();

            item_vec.push(name_url_hash);
        }

        Ok(TitlesItems {
            // title: title_vec,
            item: item_vec,
        })
    }
}
