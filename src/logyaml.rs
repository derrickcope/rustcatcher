//
// logyaml.rs
// Copyright (C) 2019 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
use serde::{Deserialize, Serialize};
use walkdir::{DirEntry, WalkDir};

#[derive(Debug, Serialize, Deserialize)]
pub struct LogConfig {
    pub log_vec: Vec<String>,
}

impl LogConfig {
    pub fn new() -> Result<LogConfig, walkdir::Error> {
        let mut log_vec: Vec<String> = Vec::new();
        let walker = WalkDir::new(".").into_iter();

        for entry in walker {
            let entry = match entry {
                Ok(file) => file,
                Err(e) => return Err(e),
            };
            if is_podcast(&entry) {
                log_vec.push(entry.file_name().to_str().unwrap().to_string());
            }
        }
        let log_config1: LogConfig = LogConfig { log_vec };
        Ok(log_config1)
    }
}
pub fn is_podcast(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.ends_with("mp3"))
        .unwrap_or(false)
}
