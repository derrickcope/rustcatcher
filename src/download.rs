//download.rs
extern crate reqwest;
extern crate rss;
use crate::feedyaml::TitlesItems;
use rayon::prelude::*;
use std::fs::{create_dir, File};
use std::io::copy;

impl TitlesItems {
    pub fn download(&mut self) {
        self.item.par_iter_mut().for_each(|item| {
            let value = item.remove("title").unwrap();
            checkdir(&value);
            item.par_iter().for_each(|(name, url)| {
                let path: String = value.clone() + "/" + name;
                let mut file = File::create(path).expect("failed to create file");
                let mut response = match reqwest::get(url.as_str()) {
                    Ok(file) => {
                        println!("downloading: {:?}", name);
                        file
                    }
                    Err(_e) => {
                        println!("no response from website url during download");
                        return;
                    }
                };
                copy(&mut response, &mut file).expect("failed to copy file to system");
            })
        })
    }
}

pub fn checkdir(dir_name: &String) {
    match create_dir(dir_name) {
        Ok(()) => {
            println!("created directory: {}", dir_name);
        }
        Err(_e) => {
            println!("directory exists: {}", dir_name);
        }
    }
}
